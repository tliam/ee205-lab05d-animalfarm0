/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file catDatabase.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//


#include <stdbool.h>
#include <stdio.h>
#include "catDatabase.h"

   bool isFixed [MAX_CATS];
   float weight [MAX_CATS];
   int numberOfCats = 0;
   char name [MAX_CATS][MAX_CAT_NAME];
   BreedType breeds[MAX_CATS];
   GenderType genders[MAX_CATS];
void initialize(){}
