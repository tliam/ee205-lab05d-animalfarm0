/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include "deleteCats.h"

void deleteAllCats(){
   for (int i = 0; i < MAX_CATS; i++){
      isFixed[i] = false;
      weight[i] = 0;
      memset(name[i],0,strlen(name[i]));
   }
   //strcpy(name[i], "");
   //memset(name,0,strlen(name));
}
