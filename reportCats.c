/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#include <stdio.h>
#include "reportCats.h"
#include <string.h>
#include <stdlib.h>

void printCat(int index){
   if (strlen(name[index])==0 || index < 0){
      printf("Bad cat [%d]\n", index);
   }else{
      printf("cat index = [%lu] name = [%s], gender=[%d], breed=[%d], isFixed[%d], weight=[%f]\n", (unsigned long)index, name[index], genders[index], breeds[index], isFixed[index], weight[index]);
   }
}


void printAllCats(){
   for(int i=0; i < MAX_CAT_NAME; i++){
      if (strlen(name[i]) != 0)
         printf("%s ", name[i]);
   }
   printf("\n");
}


int findCat(char catName[]){
   for ( int i = 0; i < MAX_CATS; i++ )
   {
      if (strcmp( name[i], catName ) == 0){
         return i;
      }
   }
   exit(0);
}

