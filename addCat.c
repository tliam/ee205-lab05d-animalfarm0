/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file addCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//

#include <stdio.h>
#include "addCat.h"
#include <string.h>

//#define DEBUG
int addCat(char catName[], GenderType gender, BreedType breed, bool fixed, float catWeight){
#ifdef DEBUG
   printf("%s\n", catName);
   printf("%d\n", gender);
   printf("%d\n", breed);
   printf("%d\n", fixed);
   printf("%f\n", catWeight);
#endif


   if (catWeight <= 0 || strlen(catName)== 0 || strlen(catName) > 30 || numberOfCats > 30)
      return 1;

   for ( int i = 0; i < MAX_CATS; i++ )
   {
      if ( strcmp( name[i], catName ) == 0 )
      {
         return 1;
      }
   }

   isFixed[numberOfCats] = fixed;
   for (int i=0; i < (int)strlen(catName); i++){
      name[numberOfCats][i] = catName[i];
   }
   breeds[numberOfCats] = breed;
   genders[numberOfCats] = gender;
   weight[numberOfCats] = catWeight;
//everything here works and this basically tells how we can set and delete names for the rest of this lab. 
#ifdef DEBUG
   for (int i = 0; i <= numberOfCats; i++){
      printf("%d\n", isFixed[i]);
      printf("%c", catName[i]);
   }
#endif
return numberOfCats++;

}

