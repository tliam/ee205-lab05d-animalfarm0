/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - AnimalFarm0 - EE 205 - Spr 2022
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
#define MAX_CATS 30
#define MAX_CAT_NAME 30
#include <stdbool.h>
extern char name [MAX_CATS][MAX_CAT_NAME];

typedef enum {UNKNOWN_GENDER, MALE, FEMALE} GenderType;
typedef enum {UNKNOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX} BreedType;

extern bool isFixed [MAX_CATS];
extern float weight [MAX_CATS];
extern int numberOfCats;

extern BreedType breeds[MAX_CATS];
extern GenderType genders[MAX_CATS];

void initialize();


