/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#include "updateCats.h"

void updateCatName(int index, char newName[]){
   for (int i = 0; i < MAX_CAT_NAME; i++){
      if (strcmp( name[i], newName ) == 0){
         return;
      }
   }
   if (index < 0 && index > MAX_CAT_NAME)
      return;

   strcpy(name[index], newName);
   return;
}
void fixCat(int index){
   if (index < 0 && index > MAX_CATS)
      return;

   isFixed[index] = true;
}
void updateCatWeight(int index, float newWeight){

   if (newWeight <= 0)
      return;

   weight[index] = newWeight;
}

