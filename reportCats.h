/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - AnimalFarm0 - EE 205 - Spr 2022
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Liam Tapper <tliam@hawaii.edu>
/// @date 15_Feb_2022
/////////////////////////////////////////////////////////////////////////////
//
#pragma once
#include <stdbool.h>
#include "catDatabase.h"

void printCat(int);
void printAllCats();
int findCat(char catName[]);
